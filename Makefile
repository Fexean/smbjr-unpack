
CXX_SRC := $(foreach dir,src, $(wildcard $(dir)/*.cpp))
CXX_FLAGS := -std=c++17 -O2
C_OBJ := src/lz.o src/huff.o
CFLAGS = -O2

all: $(C_OBJ)
	g++ $(CXX_FLAGS) $(CXX_SRC) $(C_OBJ) -o unpacker
	
clean:
	rm $(C_OBJ)
