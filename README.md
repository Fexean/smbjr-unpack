# Super Monkey Ball Jr. Unpacker

Super Monkey Ball Jr. stores much of its data in a custom filesystem inside the rom.  
This tool can extract all the individual files from an SMB JR rom and also build a rom from the extracted files.  
Currently only supports the US version of the rom.  

The LZ77 & Huffman compression code was taken from [pret's gbagfx](https://github.com/pret/pokeemerald/tree/master/tools/gbagfx)

## Building
Requirements: make, gcc and g++ with C++17 support.  
run `make` to build.  

## Usage
The tool is meant to be used from the command line. It has 3 operations: unpacking, repacking and checking.  

### Unpacking rom:
`./unpacker u <rom path> <filelist path>`  
This produces a directory containing all the extracted files, a copy of the filelist file with compression types for each extracted file and a code.bin containing the rest of the rom.  
A filelist file is needed to define what files are extracted. This repo contains (an incomplete) one for the US version of the game: files_us.txt

### Repacking rom:
`./unpacker r <directory> <output rom name>`  
This command produces a rom file from a directory containing the extracted files.  
The directory must contain a code.bin file, filelist.txt file and all the files mentioned in the filelist.txt

### Checking rom:
`./unpacker c <rom path> <filelist path>`  
This command simply checks to see if the filelist's files can be found in the rom and prints information about the files.  


## Missing files
The provided files_us.txt filelist is missing the names of 15 files. These files won't be extracted and they will be missing from any rom you build.  
Since the repacked rom seems to work fine without them, I haven't found out where the game uses them (or if they're unused).  
If you find the names for any of them, feel free to add them to files_us.txt file.  
