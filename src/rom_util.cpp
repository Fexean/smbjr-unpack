#include "rom_util.hpp"
#include <cstdlib>
#include "lz.h"
#include "huff.h"

std::vector<u8> readLZ77(GbaRom& rom, u32 addr){
	int uncompressedSize;
	addr &= 0x1FFFFFF;
	unsigned char* decompressed = LZDecompress(rom.buf + addr, rom.romSize - addr, &uncompressedSize);
	
	std::vector<u8> result(uncompressedSize);
	for(int i = 0;i<uncompressedSize;i++){
		result[i] = decompressed[i];
	}
	std::free(decompressed);
	return result;
}

std::vector<u8> readHuffman(GbaRom& rom, u32 addr){
	int uncompressedSize;
	addr &= 0x1FFFFFF;
	unsigned char* decompressed = HuffDecompress(rom.buf + addr, rom.romSize - addr, &uncompressedSize);

	std::vector<u8> result(uncompressedSize);
	for(int i = 0;i<uncompressedSize;i++){
		result[i] = decompressed[i];
	}
	std::free(decompressed);
	return result;
}

void writeLZ77(GbaRom& rom, u32 addr, std::vector<u8> data){
	addr &= 0x1FFFFFF;
	int srcSize = data.size();
	unsigned char* src = (unsigned char*)std::malloc(srcSize);
	
	int i;
	for(u8 b : data){
		src[i++] = b;
	}
	
	int compressedSize;
	unsigned char* compressed = LZCompress(src, srcSize, &compressedSize, 2);
	std::memcpy(rom.buf + addr, compressed, compressedSize);
	std::free(compressed);
	std::free(src);
}

void writeHuffman(GbaRom& rom, u32 addr, std::vector<u8> data){
	addr &= 0x1FFFFFF;
	int srcSize = data.size();
	unsigned char* src = (unsigned char*)std::malloc(srcSize);
	
	int i;
	for(u8 b : data){
		src[i++] = b;
	}
	
	int compressedSize;
	unsigned char* compressed = HuffCompress(src, srcSize, &compressedSize, 4);
	std::memcpy(rom.buf + addr, compressed, compressedSize);
	std::free(compressed);
	std::free(src);
}

bool isSupportedVersion(GbaRom& rom){
	return rom.read32(0xAC) == 0x45554c41; //us version
}