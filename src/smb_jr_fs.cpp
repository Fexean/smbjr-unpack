#include <iostream>
#include <set>
#include <filesystem>
#include <algorithm>
#include "smb_jr_fs.hpp"
#include "rom_util.hpp"

namespace fs = std::filesystem;


u32 hashFilepath(const std::string& path, u32 multiplier){
	u32 namehash = 0;
	
	for(u8 nextchar : path){
		if(nextchar >= 'A' && nextchar <= 'Z')
			nextchar = nextchar + 32;
		namehash = namehash * multiplier + nextchar;
	}
	return namehash;
}


bool openMonkeyFile(MonkeyFile& file, GbaRom& rom, const std::string& path){
	u32 filesystemPtr = getFilesystemStart(rom);
	
	file.hash = hashFilepath(path, rom.read32(filesystemPtr + 4));
	const u32 maxFileIndex = rom.read32(filesystemPtr);
	
	u32 delta = 1;
	while (delta < maxFileIndex){
		delta *= 2;
	}
  
	delta = delta / 2;
	u32 fileIndex = delta;
	
	//Find file with matching hash with binary search
	while ( delta > 0 ){
		delta /= 2;
		if ( fileIndex > maxFileIndex){
			fileIndex -= delta;
			continue;
		}

		u32 foundHash = rom.read32(filesystemPtr + 8 + 16 * (fileIndex - 1));
		if ( foundHash == file.hash ){
			u32 v5 = filesystemPtr + 16 * fileIndex - 8;
			file.type = (Filetype)rom.read32(v5 + 4);
			file.data_addr = filesystemPtr + rom.read32(v5 + 12);
			file.size = rom.read32(v5 + 8);
			file.index = fileIndex;
			return true;
		}
		if ( foundHash <= file.hash )
			fileIndex += delta;
		else
			fileIndex -= delta;
	}
	return false;
}



std::vector<u8> readFileData(GbaRom& rom, const std::string& filepath){
	MonkeyFile f;
	openMonkeyFile(f, rom, filepath);
	switch(f.type){
		case UNCOMPRESSED:
			return rom.read(f.data_addr, f.size);
		
		case LZ77_WRAM:
		case LZ77_VRAM:
			return readLZ77(rom, f.data_addr);
		
		case HUFFMAN:
			return readHuffman(rom, f.data_addr);
		
		default:
			std::cerr << "[readFile] invalid type\n";

	}
	return std::vector<u8>();
}

std::string compressionToStr(int type){
	switch(type){
		case UNCOMPRESSED: return "NONE";
		case LZ77_WRAM: return "LZ77_WRAM";
		case LZ77_VRAM: return "LZ77_VRAM";
		case HUFFMAN: return "HUFFMAN";
	}
	return "";
}

Filetype strToFiletype(const std::string& str){
	if(str == "LZ77_VRAM") return LZ77_VRAM;
	if(str == "NONE") return UNCOMPRESSED;
	if(str == "LZ77_WRAM") return LZ77_WRAM;
	if(str == "HUFFMAN") return HUFFMAN;
	return INVALID;
}

void extractFile(GbaRom& rom, const std::string& filepath, const std::string& outputpath){
	auto data = readFileData(rom, filepath);
	std::ofstream of(outputpath, std::ios::binary);
	if(of.is_open()){
		for(u8 byte : data){of.put(byte);}
		of.close();
	}
}


u32 getFilesystemStart(GbaRom& rom){
	return 0x54950; //at 0x56848 in EU rom
}

u32 getFilesystemEnd(GbaRom& rom){
	return 0x3E9BF8;
}

void MonkeyFS::addFile(const std::string& filepath, const std::string& internalName, Filetype compression){
	FileEntry f = {.compression = compression, .filepath = filepath, .internalName = internalName};
	files.push_back(f);
}

bool MonkeyFS::checkHashCollisions(){
	std::set<u32> hashes;
	for(FileEntry& f : files){
		u32 hash = hashFilepath(f.internalName, hashMultiplier);
		if(hashes.find(hash) != hashes.end()){
			std::cout << "Collision:"<< hashMultiplier << " "<<hash << " "<< f.internalName<<"\n";
			return true;
		}
		hashes.insert(hash);
	}
	return false;
}

bool MonkeyFS::findMultiplier(){
	for(hashMultiplier = 11;hashMultiplier<0xFFFFFFFF;hashMultiplier+=2){
		if(!checkHashCollisions()){
			return true;
		}
	}
	return false;
}


std::vector<u8> readVectorFromFile(const std::string& filename){
	std::vector<u8> result;
	std::ifstream f(filename, std::ios::binary);
	if(f.is_open()){
		while(!f.eof()){
			int x = f.get();
			if(f.eof())
				break;
			result.push_back(x);
		}
		f.close();
	}
	return result;
}


bool MonkeyFS::writeToRom(GbaRom& rom, u32 address){
	const u32 filesystemEnd = 0x8000000 | getFilesystemEnd(rom);
	rom.write32(address, files.size());
	rom.write32(address+4, hashMultiplier);
	
	std::vector<MonkeyFile> rawFiles;
	u32 fileStartAddr = 0x8000000 | (address + 16*files.size() + 8);
	
	//write file data to rom, create monkeyfiles with rom addresses
	for(FileEntry& f : files){
		MonkeyFile rawfile;
		rawfile.type = f.compression;
		rawfile.hash = hashFilepath(f.internalName, hashMultiplier);
		
		
		auto filedata = readVectorFromFile(f.filepath);
		rawfile.size = filedata.size();
		
		//avoid overwriting the data at the end of the rom
		if(fileStartAddr+rawfile.size+3 >= filesystemEnd && fileStartAddr < filesystemEnd){
			fileStartAddr = 0x8400000;
		}
		rawfile.data_addr = fileStartAddr;
		
		switch(f.compression){
			case UNCOMPRESSED:	rom.write(fileStartAddr, filedata); break;
			case LZ77_VRAM:	writeLZ77(rom, fileStartAddr, filedata); break;
			case LZ77_WRAM:	writeLZ77(rom, fileStartAddr, filedata); break;
			case HUFFMAN:	writeHuffman(rom, fileStartAddr, filedata); break;
			default: return false;
		}
		
		//word-align start of next file
		fileStartAddr = (fileStartAddr+rawfile.size+3) & (~3);
		rawFiles.push_back(rawfile);
	}
	
	//sort files by hash
	std::sort(rawFiles.begin(), rawFiles.end(), [](MonkeyFile a, MonkeyFile b){return a.hash < b.hash;});
	
	//insert sorted file table
	int fileindex = 1;
	for(MonkeyFile& f : rawFiles){
		rom.write32(address + 8 + 16*(fileindex-1), f.hash);
		rom.write32(address + 16 * fileindex + 4, f.data_addr - address - 0x8000000);
		rom.write32(address + 16 * fileindex, f.size);
		rom.write32(address + 16 * fileindex - 4, f.type);
		fileindex++;
	}
	return true;
}
