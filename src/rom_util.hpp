#pragma once
#include "gbarom.hpp"

std::vector<u8> readLZ77(GbaRom& rom, u32 addr);
std::vector<u8> readHuffman(GbaRom& rom, u32 addr);
void writeLZ77(GbaRom& rom, u32 addr, std::vector<u8> data);
void writeHuffman(GbaRom& rom, u32 addr, std::vector<u8> data);
bool isSupportedVersion(GbaRom& rom);