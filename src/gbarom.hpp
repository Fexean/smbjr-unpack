#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>
#include <cstdint>

typedef std::uint32_t u32;
typedef std::uint16_t u16;
typedef std::uint8_t u8;
typedef std::int32_t s32;
typedef std::int16_t s16;
typedef std::int8_t s8;


struct GbaRom{
	enum {GBAROM_BUF_SIZE = (32 * 1024 * 1024)};
	
	u8* buf;
	u32 romSize;
	
	GbaRom(){
		buf = new u8[GBAROM_BUF_SIZE];
		romSize = 0;
	}
	
	GbaRom(const std::string& filename){
		buf = new u8[GBAROM_BUF_SIZE];
		open(filename);
	}
	
	~GbaRom(){
		delete [] buf;
	}
	
	bool open(const std::string& filename){
		std::ifstream f(filename, std::ios::binary);
		if(!f.is_open()){
			return false;
		}
		bool success = open(f);
		f.close();
		return success;
	}
	
	bool open(std::istream& strm){
		strm.read((char*)buf, GBAROM_BUF_SIZE);
		romSize = strm.gcount();
		return romSize != 0;
	}
	
	bool save(const std::string& filename){
		std::ofstream f(filename, std::ios::binary);
		if(!f.is_open()){return false;}
		bool success = save(f);
		f.close();
		return success;
	}
	
	bool save(std::ostream& strm){
		strm.write((char*)buf, romSize);
		return true;
	}
	
	template<typename T>
	T read(u32 addr){
		T dst;
		addr = convertAddr(addr);
		onAccess(addr, sizeof(T));
		std::memcpy(&dst, buf + addr, sizeof(T));
		return dst;
	}
	
	template<typename T>
	void write(u32 addr, T data){
		addr = convertAddr(addr);
		onAccess(addr, sizeof(T));
		std::memcpy(buf + addr, &data, sizeof(T));
	}
	
	void write8(u32 addr, u8 data){ write<u8>(addr, data); }
	void write16(u32 addr, u16 data){ write<u16>(addr, data); }
	void write32(u32 addr, u32 data){ write<u32>(addr, data); }
	u8 read8(u32 addr){ return read<u8>(addr); }
	u16 read16(u32 addr){ return read<u16>(addr); }
	u32 read32(u32 addr){ return read<u32>(addr); }
	
	std::string readStr(u32 addr, u32 len){
		std::string result(len, ' ');
		addr = convertAddr(addr);
		onAccess(addr, len);
		for(int i = 0;i<len;i++)result[i] = read8(addr+i);
		return result;
	}
	
	void writeStr(u32 addr, const std::string& str){
		addr = convertAddr(addr);
		onAccess(addr, str.size());
		std::memcpy(buf + addr, str.c_str(), str.size());
	}
	
	void write(u32 addr, std::vector<u8> data){
		for(u8 b : data){
			write8(addr++, b);
		}
	}
	
	std::vector<u8> read(u32 addr, u32 len){
		std::vector<u8> result;
		for(int i = 0;i<len;i++){
			result.push_back(read8(addr+i));
		}
		return result;
	}
	
	private:
	
	inline void onAccess(u32 addr, u32 size){
		romSize = std::max(romSize, size+addr);
	}
	
	inline u32 convertAddr(u32 addr){
		return addr &= 0x1FFFFFF;
	}
};
