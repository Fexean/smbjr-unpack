#include "gbarom.hpp"

//filesystem structure:
//0x0 - file count
//0x4 - multiplier used to calculate filename hashes
//0x8 - array of 16-byte file structures, sorted by hash
//??? - actual file data


enum Filetype{
	UNCOMPRESSED, LZ77_WRAM, LZ77_VRAM, HUFFMAN, INVALID
};

struct MonkeyFile{
	Filetype type;
	u32 data_addr;
	u32 size;
	u32 hash;
	u32 index;
};


struct FileEntry{
	Filetype compression;
	std::string filepath;
	std::string internalName;
};

struct MonkeyFS{
	u32 hashMultiplier;
	std::vector<FileEntry> files;
	
	bool checkHashCollisions();
	bool findMultiplier();
	bool writeToRom(GbaRom& rom, u32 address);
	void addFile(const std::string& filepath, const std::string& internalName, Filetype compression);
};


u32 hashFilepath(const std::string& path, u32 multiplier);
bool openMonkeyFile(MonkeyFile& file, GbaRom& rom, const std::string& path);
std::vector<u8> readFileData(GbaRom& rom, const char* filepath);
void extractFile(GbaRom& rom, const std::string& filepath, const std::string& outputpath);

std::string compressionToStr(int type);
Filetype strToFiletype(const std::string& str);

u32 getFilesystemStart(GbaRom& rom);
u32 getFilesystemEnd(GbaRom& rom);
