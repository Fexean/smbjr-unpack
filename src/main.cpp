#include <filesystem>
#include <iostream>
#include <algorithm>
#include <string>
#include "gbarom.hpp"
#include "smb_jr_fs.hpp"
#include "rom_util.hpp"

namespace fs = std::filesystem;



bool writeVectorToFile(const std::string& filename, const std::vector<u8>& data){
	std::ofstream of(filename, std::ios::binary);
	if(of.is_open()){
		for(u8 byte : data){of.put(byte);}
		of.close();
		return true;
	}
	return false;
}


void checkMode(const std::string& listfile, GbaRom& rom){
	if(!isSupportedVersion(rom)){
		std::cerr << "Unsupported rom version!\n";
		return;
	}
	
	std::ifstream f(listfile);
	if(!f.is_open()){
		std::cerr << "Unable to open list file:"<<listfile<<"\n";
		return;
	}
	
	std::cout <<std::hex <<"PATH\tHASH\tINDEX\tROM LOCATION\tCOMPRESSION\n";
	
	u32 multiplier = rom.read32(getFilesystemStart(rom) + 4);
	int found = 0;
	int notFound = 0;
	std::string line;
	
	while(std::getline(f, line)){
		if(!line.size()){continue;}
		MonkeyFile file;
		if(openMonkeyFile(file, rom, line)){
			std::cout << line <<'\t'<<file.hash<< '\t'<< file.index <<'\t'<< file.data_addr << '\t'<< compressionToStr(file.type) <<'\n';
			found++;
		}else{
			std::cout << line <<'\t'<<file.hash<< "\tFILE NOT FOUND!\n";
			notFound++;
		}
		
	}
	
	std::cout << "\nFound "<<std::dec<<found <<'/'<< found+notFound<<" files in the list.\n"
	<< "ROM Filecount: "<< rom.read32(getFilesystemStart(rom)) << "\n"
	<< "ROM hash multiplier: " << multiplier << "\n";
}



void readMode(const std::string& listfile, GbaRom& rom, std::string romName){
	if(!isSupportedVersion(rom)){
		std::cerr << "Unsupported rom version!\n";
		return;
	}
	std::string extractionDir = romName + "_fs";
	fs::create_directory(extractionDir);
	
	std::ifstream f(listfile);
	if(!f.is_open()){
		std::cerr << "Unable to open list file:"<<listfile<<"\n";
		return;
	}
	
	std::ofstream outputList(extractionDir + "/filelist.txt");
	if(!outputList.is_open()){
		std::cerr << "Unable to create list file:"<<extractionDir<<"\\filelist.txt\n";
		return;
	}
	
	
	std::string line;
	std::string outputpath;
	while(std::getline(f, line)){
		if(!line.size()){continue;}
		u32 hash = hashFilepath(line, rom.read32(getFilesystemStart(rom) + 4));
		MonkeyFile file;
		std::error_code ec;
		outputpath = extractionDir + '/' + line;
		std::replace(outputpath.begin(), outputpath.end(), '\\', '/');
		std::cout << outputpath << "\n";
		if(openMonkeyFile(file, rom, line)){
			const std::string dir = outputpath.substr(0, outputpath.find_last_of('/'));
			if(!fs::exists(dir) && !fs::create_directories(dir, ec)){
				std::cout << "Unable to create directory:"<<dir<<"\nReason:" << ec.message() << "\n";;
			}else{
				extractFile(rom, line, outputpath);
				outputList << line << ' ' << compressionToStr(file.type) << "\n";
			}
		}else{
			std::cout << line <<"\t"<<hash<< "\tFILE NOT FOUND!\nAborted!\n";
			return;
		}
	}
	
	fs::copy_file(romName, extractionDir + "/code.bin", fs::copy_options::overwrite_existing);
}



void repackMode(std::string directoryName, std::string outputName){
	GbaRom codebin;
	if(!codebin.open(directoryName + "/code.bin")){
		std::cout << "Directory must contain code.bin\n";
		return;
	}

	std::ifstream f(directoryName + "/filelist.txt");
	if(!f.is_open()){
		std::cout << "Failed to open "<<directoryName<<"\\filelist.txt\n";
		return;
	}
	
	MonkeyFS filesystem;
	std::string line;
	while(std::getline(f, line)){
		if(!line.size() || line.find(' ') == std::string::npos){continue;}
		
		std::string internalName = line.substr(0, line.find(' '));
		Filetype type = strToFiletype(line.substr(line.find(' ') + 1));
		if(type == INVALID){
			std::cerr << "Invalid compression type: " << line.substr(line.find(' ') + 1) <<"\nOn line:" << line << "\n";
			return;
		}
		
		std::string filepath = directoryName + '/' + internalName;
		std::replace(filepath.begin(), filepath.end(), '\\', '/');
		filesystem.addFile(filepath, internalName, type);
	}
	
	std::cout << "Searching for hash multiplier...\n";
	if(!filesystem.findMultiplier()){
		std::cout << "Failed to find a hash multiplier.\n";
		return;
	}
	std::cout << "Multiplier: "<< filesystem.hashMultiplier << '\n';
	
	std::cout << "Building ROM...\n";
	filesystem.writeToRom(codebin, getFilesystemStart(codebin));
	if(!codebin.save(outputName)){
		std::cout << "Failed to save ROM:" << outputName << "\n";
	}
}



void printUsage(const std::string& name){
	std::cout << "USAGE: "<<name<<" <mode> <arguments>\n"
	<< "Available modes: (u)npack, (r)epack, (c)heck\n\n"
	<< "UNPACK: "<< name << " u <rom> <list>\nWhere the <rom> is your rom file and <list> a file containing the names of the files to extract, 1 per line.\n\n"
	<< "REPACK: "<< name << " r <dir> <out>\nWhere <dir> is the directory containing the unpacked files and <out> is the name of the rom to be created.\n\n"
	<< "CHECK: "<< name << " c <rom> <list>\nWhere the <rom> is your rom file and <list> a file containing the names of the files to check, 1 per line.\n\n"
	;
}


int main(int argc, char* argv[]){
	GbaRom rom;
	if(argc != 4){
		printUsage(argv[0]);
		return 0;
	}
	
	switch(argv[1][0]){
		case 'c':	//CHECK
			if(!rom.open(argv[2])){
				std::cerr << "Failed to open rom!\n";
				return 0;
			}
			checkMode(argv[3], rom);
		break;
		
		case 'u': //UNPACK
			if(!rom.open(argv[2])){
				std::cerr << "Failed to open rom!\n";
				return 0;
			}
			readMode(argv[3], rom, argv[2]);
		break;
		
		case 'r': //REPACK
			repackMode(argv[2], argv[3]);
		break;
		
		default:
			printUsage(argv[0]);
			std::cout << "Invalid mode:"<<argv[1]<<"\n";
	}
	
}
