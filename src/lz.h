// Copyright (c) 2015 YamaArashi

#ifndef LZ_H
#define LZ_H

#ifdef __cplusplus
extern "C"
{
#endif
unsigned char *LZDecompress(unsigned char *src, int srcSize, int *uncompressedSize);
unsigned char *LZCompress(unsigned char *src, int srcSize, int *compressedSize, const int minDistance);

#ifdef __cplusplus
}
#endif
#endif // LZ_H
